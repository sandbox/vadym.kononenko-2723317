<?php

/**
 * @file
 * Additional conditions and actions for Rules.
 */

function entity_extra_rules_rules_action_info() {
  return array(
    'entity_extra_rules_get_entity_url_action' => array(
      'label' => t('Get the entity URL'),
      'parameter' => array(
        'entity' => array(
          'type' => 'entity',
          'label' => t('Entity'),
        ),
      ),
      'provides' => array(
        'entity_url' => array(
          'type' => 'text',
          'label' => t('Entity URL'),
        ),
      ),
      'group' => t('Entities'),
    ),
    'entity_extra_rules_get_entity_identifiers_action' => array(
      'label' => t('Get the entity identifiers'),
      'parameter' => array(
        'entity' => array(
          'type' => 'entity',
          'label' => t('Entity'),
        ),
      ),
      'provides' => array(
        'entity_type' => array(
          'type' => 'text',
          'label' => t('Entity Type'),
        ),
        'entity_id' => array(
          'type' => 'text',
          'label' => t('Entity ID'),
        ),
      ),
      'group' => t('Entities'),
    ),
    'entity_extra_rules_get_entity_owner_action' => array(
      'label' => t('Get the entity owner'),
      'parameter' => array(
        'entity' => array(
          'type' => 'entity',
          'label' => t('Entity'),
        ),
      ),
      'provides' => array(
        'entity_owner' => array(
          'type' => 'user',
          'label' => t('Entity owner'),
        ),
      ),
      'group' => t('Entities'),
    ),
  );
}

function entity_extra_rules_get_entity_identifiers_action($wrapper) {
  $entity_type = $wrapper->type();
  $entity_id   = $wrapper->getIdentifier();

  return array(
    'entity_type' => $entity_type,
    'entity_id'   => $entity_id,
  );
}

function entity_extra_rules_get_entity_url_action($wrapper) {
  // @see: class EntityDefaultMetadataController at the file 'entity/entity.info.inc'
  // This is the only property set by 'entity' module global and with explicitly name.
  $entity_uri_path = $wrapper->url->value();

  return array('entity_url' => $entity_uri_path);
}

function entity_extra_rules_get_entity_owner_action($wrapper) {
  $user = NULL;

  // Unwrap an entity.
  $entity = $wrapper->value();
  // ... and get the 'uid' property.
  $user_id = isset($entity->uid) ? $entity->uid : FALSE;

  if ($user_id) {
    $user = entity_metadata_wrapper('user', $user_id);
  }

  return array('entity_owner' => $user);
}
